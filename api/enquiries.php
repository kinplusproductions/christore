<?php
require 'initialize.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require 'PHPMailer/src/Exception.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'enquiries';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        $treat = isset($_GET['treat']) ? $_GET['treat'] : '';
        if ($u == 2) {
            if ($count == 1 && $treat == '') {
                echo json_encode(countRecords($table));
            } elseif ($count == 1 && $treat == 0) {
                echo json_encode(countRecords($table, 'treated=:treated', ['treated'=>$treat]));
            } elseif ($count == 1 && $treat == 1) {
                echo json_encode(countRecords($table, 'treated=:treated', ['treated'=>$treat]));
            } else {
                echo json_encode(selectRecords($table, [], "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;

    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $id = $data['id'] = uuid();
        $column = "id, fullname, email, phoneno, subject, message, treated, treated_by, category";
        $value = ":id, :fullname, :email, :phoneno, :subject, :message, :treated, :treated_by, :category";
        $res = insertRecord($table, $column, $value, $data);
        if ($res) {
            $mail = new PHPMailer(TRUE);
            $mail_execution = mail_user($mail, $data);
            echo json_encode(['ok' => 1, 'mail_msg' => $mail_execution['message']]);
        }else{
            echo json_encode(['ok' => 0, 'mail_msg' => $mail_execution['message']]);
        }
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        // $id = $data['id'];
        $res = '';
        extract($data);
        switch ($type) {
            case 'treated':
                $column = "treated=:treated, treated_by=:treated_by";
                $update_data = ['id' => $id, 'treated' => $treated, 'treated_by' => $treated_by];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id= :id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
