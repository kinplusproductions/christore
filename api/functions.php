<?php

function countRecords($table, $where_clause = 1, $data = [])
{
    global $pdo;
    $sql = "SELECT COUNT(*) AS total FROM $table WHERE $where_clause ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute($data);
    return $stmt->fetch();
}

function selectRecords($table, $json_arr = [], $where_clause = 1, $data = [], $column = "*")
{
    global $pdo;
    $sql = "SELECT $column FROM $table WHERE $where_clause ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute($data);
    $res = [];
    while ($row = $stmt->fetch()) {
        $res[] = decodeJson($row, $json_arr);
    }
    return $res;
}

function selectRecord($table, $json_arr = [], $where_clause = 1, $data = [], $column = "*")
{
    global $pdo;
    $sql = "SELECT $column FROM $table WHERE $where_clause ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute($data);
    $res = $stmt->fetch();
    return decodeJson($res, $json_arr);
}

function insertRecord($table, $column, $value, $data = [])
{
    global $pdo;
    $sql = "INSERT INTO $table($column) VALUES ($value) ";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute($data);
}

function updateRecord($table, $column, $where_clause, $data = [])
{
    global $pdo;
    $sql = "UPDATE $table SET $column WHERE $where_clause ";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute($data);
}

function deleteRecord($table, $where_clause, $data = [])
{
    global $pdo;
    $sql = "DELETE FROM $table WHERE $where_clause ";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute($data);
}

function truncateRecord($table)
{
    global $pdo;
    $sql = "TRUNCATE TABLE $table ";
    $stmt = $pdo->prepare($sql);
    return $stmt->execute();
}

function decodeJson($record, $json_arr)
{
    foreach ($json_arr as $field) {
        $record[$field] = json_decode($record[$field]);
    }
    return $record;
}

function encodeJson($record, $json_arr)
{
    foreach ($json_arr as $field) {
        $record[$field] = json_encode($record[$field]);
    }
    return $record;
}

function user_exists($table, $bd1)
{
    $applicants = selectRecords($table, "biodata, application_no, submitted");
    $res = false;
    foreach ($applicants as $a) {
        $bd2 = json_decode($a->biodata);
        if (($bd1->surname == $bd2->surname && $bd1->other_names == $bd2->other_names) || ($bd1->mobile_no == $bd2->mobile_no) || ($bd1->email == $bd2->email)) {
            $res = json_encode(['biodata' => $a->biodata, 'application_no' => $a->application_no, 'submitted' => $a->submitted]);
            break;
        }
    }
    return $res;
}

function mail_user($mail, $data)
{
    $fullname = $data['fullname'];
    $usermail = $data['email'];
    $phoneno = $data['phoneno'];

    $mail->setFrom('info@christoreservices.com', 'Christore Website');
    // $mail->Sender = 'enquiries@christoreservices.com';
    $mail->addAddress('mittiwa@gmail.com', 'Christore Manager');
    $mail->addReplyTo("info@christoreservices.com");
    $mail->isHTML(true);
    $mail->Subject = 'Enquiries From Christore Website';
    $mail->Body = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style>
		html, body {
				margin: 0px;
				padding: 0px;
			}
			.text-center {
				text-align: center;
			}
			.thm-text {
				color:  #ff5722;
			}
			.bg-dark {
				background-color:  #000;
			}
			.row {
				display: flex;
				flex-wrap: wrap;
				justify-content: center;
				align-items: center;
			}
			.col {
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			img {
				max-width: 100%;
			}
			.container{
				margin-left: auto;
				margin-right: auto;
				padding-left: 1rem;
				padding-right: 1rem;
			}
      .link {
        color: blue;
      }
		</style>
		<title>Message from ' . $fullname . '</title>
		</head>
		<body>
			<main class="container">
				<h4>Dear Christore Manager,</h4>
				<p>A User Sent An Enquiry Through Christore Website. Please Login To Christore Website Admin Page (https://christoreservices.com/admin-login) So As To Respond. </p>
				<p>User <b>Email </b> is: <b class="thm-text">' . $usermail . '</b>.</p>
				<p>User <b>Phone Number </b> is: <b class="thm-text">' . $phoneno . '</b>.</p>
		        <h4><b>Thanks</b></h4>
			</main>
			</body>
		</html>
    ';
    $mail->AltBody = 'A User with email: ' . $usermail . ' Sent An Enquiry Message Through Christore Website. Please Login To Christore Website Admin Page (https://christoreservices.com/admin-login) So As To Respond.';

    $mail->isSMTP();
    $mail->Host = 'mail.christoreservices.com';
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Username = 'info@christoreservices.com';
    $mail->Password = 'christoreservices2021';
    $mail->Port = 465;

    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true,
        ),
    );

    if (!$mail->send()) {
        $msg = 'Mail could not be sent.';
        $msg .= 'Mailer Error: ' . $mail->ErrorInfo;
        $mailsent = 0;
    } else {
        $msg = 'Mail Sent Successfully.';
        $mailsent = 1;
    }
    return ['mailsent' => $mailsent, 'message' => $msg];
}

function uuid()
{
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

        // 32 bits for "time_low"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),

        // 16 bits for "time_mid"
        mt_rand(0, 0xffff),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand(0, 0x0fff) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand(0, 0x3fff) | 0x8000,

        // 48 bits for "node"
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
}
