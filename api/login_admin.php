<?php 
require 'initialize.php';

$table = 'admin';
$data = json_decode(file_get_contents("php://input"), true);
extract($data);

$res = selectRecord($table, [], "username=:username AND passwd=:passwd", ['username' => $username, 'passwd' => $passwd]);

if ($res) {
	echo $res['is_active']==1 ? json_encode($res):json_encode('You are currently deactivated on this platform') ;
}else{
	echo json_encode('Incorrect Login Details: Please Enter Correct Details');
}

?>