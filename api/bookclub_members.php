<?php
require 'initialize.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';
require 'PHPMailer/src/Exception.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'bookclub_members';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, $json_fields, "1 ORDER BY created_on ASC"));
            }
        } else {
            echo json_encode([]);
        }
        break;

    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true); // Get raw posted data as assoc array
        $email = $data['email'];
        $phoneno = $data['phoneno'];
        $res2 = selectRecord($table, [], "email=:email OR phoneno=:phoneno", ['email' => $email, 'phoneno' => $phoneno]);
        if ($res2) {
            if ($email == $res2['email']) {
                echo json_encode('A User With This Email Already Exists: Please Login To Continue Your Application');
            } elseif ($phoneno == $res2['phoneno']) {
                echo json_encode('A User With This Phone Number Already Exists: Please Login To Continue Your Application');
            }
            return;
        }
        $id = $data['id'] = uuid();
        $column = 'id, fullname, email, phoneno';
        $value = ':id, :fullname, :email, :phoneno';
        $res = insertRecord($table, $column, $value, $data);
            if ($res) {
                $mail = new PHPMailer(TRUE);
                $mail_execution = mail_user($mail, $data);
                echo json_encode(['ok' => 1, 'mail_msg' => $mail_execution['message']]);
            }else{
                echo json_encode(['ok' => 0, 'mail_msg' => $mail_execution['message']]);
            }
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true); // Get raw posted data
        $id = $data['id'];
        $res = '';
        extract($data);
        $column = "fullname=:fullname,
        email=:email,
        phoneno=:phoneno
        ";
        $update_data = [
            'id' => $id,
            'fullname' => $fullname,
            'email' => $email,
            'phoneno' => $phoneno,
        ];

        $res = updateRecord($table, $column, "id=:id", $update_data);

        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;

    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];

        extract(selectRecord($table, $json_fields, "id= :id", ['id' => $id]));
        $res = deleteRecord($table, "id= :id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
