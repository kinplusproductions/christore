<?php
require 'initialize.php';

$method = $_SERVER['REQUEST_METHOD'];
$table = 'admin';
$json_fields = [];
$res = '';

switch ($method) {
    case 'GET':
        $u = isset($_GET['u']) ? $_GET['u'] : '';
        $count = isset($_GET['count']) ? $_GET['count'] : '';
        if ($u == 2) {
            if ($count == 1) {
                echo json_encode(countRecords($table));
            } else {
                echo json_encode(selectRecords($table, $json_fields, "1 ORDER BY created_on"));
            }
        } else {
            echo json_encode([]);
        }
        break;

    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $id = $data['id'] = uuid();
        $column = "id, fullname, username, passwd, is_active, admin_level";
        $value = ":id, :fullname, :username, :passwd, :is_active, :admin_level";
        $res = insertRecord($table, $column, $value, $data);
        if ($res) {
            echo json_encode(selectRecord($table, $json_fields, "id=:id", ['id' => $id]));
        } else {
            echo json_encode('Unable To Add Admin');
        }
        break;

    case 'PUT':
    case 'PATCH':
        $data = json_decode(file_get_contents("php://input"), true);
        // $id = $data['id'];
        $res = '';
        extract($data);
        switch ($type) {
            case 'superadmin':
                $column = "fullname=:fullname,
                username=:username,
                passwd=:passwd,
                is_active=:is_active,
                admin_level=:admin_level
                ";
                $update_data = ['id' => $id, 'fullname' => $fullname,
                    'username' => $username,
                    'passwd' => $passwd,
                    'is_active' => $is_active,
                    'admin_level' => $admin_level];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            case 'admin':
                $column = "fullname=:fullname,
                username=:username,
                passwd=:passwd";
                $update_data = ['id' => $id, 'fullname' => $fullname,
                    'username' => $username,
                    'passwd' => $passwd];
                $res = updateRecord($table, $column, "id=:id", $update_data);
                break;
            default:
                break;
        }
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    case 'DELETE':
        $id = $_SERVER['QUERY_STRING'];
        $res = deleteRecord($table, "id= :id", ['id' => $id]);
        echo $res ? json_encode(['ok' => 1]) : json_encode(['ok' => 0]);
        break;
    default:
        break;
}
