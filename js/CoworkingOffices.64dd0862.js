(window["webpackJsonp"]=window["webpackJsonp"]||[]).push([["CoworkingOffices"],{1681:function(t,e,i){},"1b62":function(t,e,i){"use strict";i.d(e,"a",(function(){return s}));var s={data:function(){return{snackbar:!1,snackMsg:"",snackColor:"",loading:!1}},methods:{displayMsg:function(t){var e=arguments.length>1&&void 0!==arguments[1]?arguments[1]:"success",i=arguments.length>2&&void 0!==arguments[2]?arguments[2]:"loading",s=!(arguments.length>3&&void 0!==arguments[3])||arguments[3];this.snackMsg=t,this.snackColor=e,this.snackbar=s,this[i]=!1}}}},"4bd4":function(t,e,i){"use strict";var s=i("5530"),a=(i("caad"),i("2532"),i("07ac"),i("4de4"),i("159b"),i("7db0"),i("58df")),r=i("7e2b"),n=i("3206");e["a"]=Object(a["a"])(r["a"],Object(n["b"])("form")).extend({name:"v-form",provide:function(){return{form:this}},inheritAttrs:!1,props:{disabled:Boolean,lazyValidation:Boolean,readonly:Boolean,value:Boolean},data:function(){return{inputs:[],watchers:[],errorBag:{}}},watch:{errorBag:{handler:function(t){var e=Object.values(t).includes(!0);this.$emit("input",!e)},deep:!0,immediate:!0}},methods:{watchInput:function(t){var e=this,i=function(t){return t.$watch("hasError",(function(i){e.$set(e.errorBag,t._uid,i)}),{immediate:!0})},s={_uid:t._uid,valid:function(){},shouldValidate:function(){}};return this.lazyValidation?s.shouldValidate=t.$watch("shouldValidate",(function(a){a&&(e.errorBag.hasOwnProperty(t._uid)||(s.valid=i(t)))})):s.valid=i(t),s},validate:function(){return 0===this.inputs.filter((function(t){return!t.validate(!0)})).length},reset:function(){this.inputs.forEach((function(t){return t.reset()})),this.resetErrorBag()},resetErrorBag:function(){var t=this;this.lazyValidation&&setTimeout((function(){t.errorBag={}}),0)},resetValidation:function(){this.inputs.forEach((function(t){return t.resetValidation()})),this.resetErrorBag()},register:function(t){this.inputs.push(t),this.watchers.push(this.watchInput(t))},unregister:function(t){var e=this.inputs.find((function(e){return e._uid===t._uid}));if(e){var i=this.watchers.find((function(t){return t._uid===e._uid}));i&&(i.valid(),i.shouldValidate()),this.watchers=this.watchers.filter((function(t){return t._uid!==e._uid})),this.inputs=this.inputs.filter((function(t){return t._uid!==e._uid})),this.$delete(this.errorBag,e._uid)}}},render:function(t){var e=this;return t("form",{staticClass:"v-form",attrs:Object(s["a"])({novalidate:!0},this.attrs$),on:{submit:function(t){return e.$emit("submit",t)}}},this.$slots.default)}})},"5a93":function(t,e,i){"use strict";var s=function(){var t=this,e=t.$createElement,i=t._self._c||e;return i("div",[i("v-form",{ref:"dform",attrs:{"lazy-validation":""},on:{submit:function(t){t.preventDefault()}}},[i("v-text-field",{attrs:{rules:t.nameRules,label:"Full Name",counter:"50",outlined:"",required:"",rounded:""},model:{value:t.dform.fullname,callback:function(e){t.$set(t.dform,"fullname","string"===typeof e?e.trim():e)},expression:"dform.fullname"}}),i("v-text-field",{attrs:{outlined:"",rules:t.emailRules,label:"E-mail",required:"",rounded:""},model:{value:t.dform.email,callback:function(e){t.$set(t.dform,"email","string"===typeof e?e.trim():e)},expression:"dform.email"}}),i("v-text-field",{attrs:{outlined:"",counter:15,rules:t.phoneRules,label:"Phone Number",required:"",rounded:""},model:{value:t.dform.phoneno,callback:function(e){t.$set(t.dform,"phoneno","string"===typeof e?e.trim():e)},expression:"dform.phoneno"}}),i("v-text-field",{attrs:{rules:t.subjectRules,outlined:"",label:"Subject",required:"",rounded:""},model:{value:t.dform.subject,callback:function(e){t.$set(t.dform,"subject","string"===typeof e?e.trim():e)},expression:"dform.subject"}}),i("v-textarea",{attrs:{"background-color":"grey lighten-2",rules:t.messageRules,label:"message",outlined:"",required:"",rounded:""},model:{value:t.dform.message,callback:function(e){t.$set(t.dform,"message","string"===typeof e?e.trim():e)},expression:"dform.message"}}),i("v-btn",{staticClass:"mr-4 button",attrs:{color:"primary",loading:t.loading},on:{click:t.submitForm}},[t._v(" Submit ")])],1),i("v-snackbar",{attrs:{color:t.snackColor},scopedSlots:t._u([{key:"action",fn:function(e){var s=e.attrs;return[i("v-btn",t._b({attrs:{color:"pink",text:""},on:{click:function(e){t.snackbar=!1}}},"v-btn",s,!1),[t._v(" Close ")])]}}]),model:{value:t.snackbar,callback:function(e){t.snackbar=e},expression:"snackbar"}},[t._v(" "+t._s(t.snackMsg)+" ")])],1)},a=[],r=i("dde5"),n=i("1b62"),o={name:"ContactForm",props:{category:{type:String,default:"general"}},mixins:[n["a"]],data:function(){return{dform:this.initializeForm(),nameRules:[function(t){return!!t||"Name is required"},function(t){return t&&t.length<=50||"Name must be less than 50 characters"}],messageRules:[function(t){return!!t||"Message is required"}],subjectRules:[function(t){return t&&t.length<=50||"Subject must be less than 50 characters"}],emailRules:[function(t){return!!t||"E-mail is required"},function(t){return/.+@.+\..+/.test(t)||"E-mail must be valid"}],phoneRules:[function(t){return!!t||"Phone number is required"},function(t){return t&&t.length<=15||"Name must be less than 15 characters"}]}},methods:{submitForm:function(){var t=this;this.$refs.dform.validate()&&(this.loading=!0,r["a"].post("/enquiries",this.dform).then((function(e){e.data.ok?(t.dform=t.initializeForm(),t.displayMsg("Thanks! Your Message has been submitted successfully, We will get back to you"),t.$refs.dform.resetValidation()):t.displayMsg(e.data,"error")})).catch((function(e){return t.displayMsg(e.message,"error")})))},initializeForm:function(){return{id:"",fullname:"",email:"",phoneno:"",subject:"",message:"",treated:0,treated_by:"",category:this.category}}}},u=o,c=i("2877"),l=i("6544"),d=i.n(l),f=i("8336"),m=i("4bd4"),h=i("2db4"),v=i("8654"),p=i("5530"),b=(i("a9e3"),i("1681"),i("58df")),g=Object(b["a"])(v["a"]),y=g.extend({name:"v-textarea",props:{autoGrow:Boolean,noResize:Boolean,rowHeight:{type:[Number,String],default:24,validator:function(t){return!isNaN(parseFloat(t))}},rows:{type:[Number,String],default:5,validator:function(t){return!isNaN(parseInt(t,10))}}},computed:{classes:function(){return Object(p["a"])({"v-textarea":!0,"v-textarea--auto-grow":this.autoGrow,"v-textarea--no-resize":this.noResizeHandle},v["a"].options.computed.classes.call(this))},noResizeHandle:function(){return this.noResize||this.autoGrow}},watch:{lazyValue:function(){this.autoGrow&&this.$nextTick(this.calculateInputHeight)},rowHeight:function(){this.autoGrow&&this.$nextTick(this.calculateInputHeight)}},mounted:function(){var t=this;setTimeout((function(){t.autoGrow&&t.calculateInputHeight()}),0)},methods:{calculateInputHeight:function(){var t=this.$refs.input;if(t){t.style.height="0";var e=t.scrollHeight,i=parseInt(this.rows,10)*parseFloat(this.rowHeight);t.style.height=Math.max(i,e)+"px"}},genInput:function(){var t=v["a"].options.methods.genInput.call(this);return t.tag="textarea",delete t.data.attrs.type,t.data.attrs.rows=this.rows,t},onInput:function(t){v["a"].options.methods.onInput.call(this,t),this.autoGrow&&this.calculateInputHeight()},onKeyDown:function(t){this.isFocused&&13===t.keyCode&&t.stopPropagation(),this.$emit("keydown",t)}}}),w=Object(c["a"])(u,s,a,!1,null,null,null);e["a"]=w.exports;d()(w,{VBtn:f["a"],VForm:m["a"],VSnackbar:h["a"],VTextField:v["a"],VTextarea:y})},"5db8":function(t,e,i){"use strict";i.r(e);var s=function(){var t=this,e=t.$createElement,i=t._self._c||e;return i("div",[i("bread-crumb",{attrs:{items:t.items}}),i("v-container",{staticClass:"mt-5"},[i("P",[t._v("CHRISTORE OFFICE HUB is a Co-working Offices based in Ado-Ekiti, Ekiti State and focuses on corporate experiences, inspiring shared office spaces, convenient virtual offices and well equipped private offices. The hub offers a dynamic community for people andbusiness to grow together.")]),i("P",[t._v("It is not just a place to work but rather a movement of innovation. Business center for early stage ventures that have uncertain headcount projections operate entirely from Co-working Offices due to the spatial flexibility Office.")]),i("P",[t._v("Our fully furnished office provides businesses with the opportunity to leverage expertise without making a capital-intensive investment. We also offer counseling, coaching, consulting, and training with essential")]),i("h3",{staticClass:"mt-10 text-center"},[t._v("OUR SERVICES")]),i("v-row",{staticClass:"mt-6",attrs:{justify:"center"}},[i("v-col",[i("v-img",{attrs:{transition:"scroll-x-transition",src:"img/conference.jpg",height:"300",width:"500"}})],1),i("v-col",{attrs:{"align-self":"center"}},[i("v-card-title",{staticClass:"primary--text"},[i("b",[t._v(" FLEXI SPACES ")])]),i("p",[t._v(" Bring your own gear, set up where you like, grab a desk and start work, conference room available on request. "),i("b",[t._v("AS LOW AS 1000/MONTH")])])],1)],1),i("v-row",{staticClass:"mt-6"},[i("v-col",{attrs:{"align-self":"center",order:"1","order-sm":"1"}},[i("v-card-title",{staticClass:"primary--text"},[i("b",[t._v(" DEDICATED DESKS")])]),i("p",[t._v(" Your very own standard desk to mess up as much as you like with access to conference room, secretarial services and business address. "),i("b",[t._v(" AS LOW AS 25,000/MONTH")])])],1),i("v-col",{attrs:{"order-sm":"2"}},[i("v-img",{attrs:{src:"img/dedicated.png",height:"300",width:"500"}})],1)],1),i("v-row",{staticClass:"mt-10"},[i("v-col",[i("v-img",{attrs:{src:"img/executive.png",height:"300",width:"500"}})],1),i("v-col",{attrs:{"align-self":"center"}},[i("v-card-title",{staticClass:"primary--text"},[i("b",[t._v("EXECUTIVE OFFICES ")])]),i("p",[t._v(" Well furnished and prepared to host a number of visitors at a time, these offices are in different sizes and different designs to suit your business needs, with access to conference room, secretarial services and business address. "),i("b",[t._v(" AS LOW AS 35,000/MONTH")])])],1)],1),i("v-row",{staticClass:"mt-6"},[i("v-col",{attrs:{"align-self":"center",order:"1","order-sm":"1"}},[i("v-card-title",{staticClass:"primary--text"},[i("b",[t._v("SECRETARIAL SERVICE/ Virtual Offices ")])]),i("p",[t._v(" Specialized and experience secretary to handle all mails and meetings without being physically present. Also known as the virtual office, the service is available to individuals away from the state while answering to the office demands. "),i("b",[t._v(" AS LOW AS 7,500/MONTH")])])],1),i("v-col",{attrs:{"order-sm":"2"}},[i("v-img",{attrs:{src:"img/sec.jpg",height:"300",width:"500"}})],1)],1)],1),i("h3",{staticClass:"text-center my-6"},[t._v(" RENTALS: PROJECTORS/SCREENS, CONFERENCE ROOM AND OTHER EQUIPMENT ARE AVAILABLE FOR HOURLY RENTALS. ")]),i("h3",{staticClass:"text-center grey lighten-3 pa-8"},[t._v(" NOTE* ALL PRICES ARE NEGOTIABLE WITH TERMS AND CONDITIONS ")]),i("v-container",[i("h2",{staticClass:"text-center mt-7"},[t._v("To Make More Enquries")]),i("v-row",{attrs:{justify:"center"}},[i("v-col",{attrs:{cols:"12",md:"7",lg:"8"}},[i("contact-form",{attrs:{category:"offices"}})],1)],1)],1)],1)},a=[],r=i("85e0"),n=i("5a93"),o={name:"CoworkingSpaces",components:{BreadCrumb:r["a"],ContactForm:n["a"]},data:function(){return{items:[{text:"Home",disabled:!1,link:"Home"},{text:"Co-Working Offices",disabled:!0,link:"CoworkingOffices"}]}}},u=o,c=i("2877"),l=i("6544"),d=i.n(l),f=i("99d9"),m=i("62ad"),h=i("a523"),v=i("adda"),p=i("0fd9"),b=Object(c["a"])(u,s,a,!1,null,null,null);e["default"]=b.exports;d()(b,{VCardTitle:f["d"],VCol:m["a"],VContainer:h["a"],VImg:v["a"],VRow:p["a"]})},"85e0":function(t,e,i){"use strict";var s=function(){var t=this,e=t.$createElement,i=t._self._c||e;return i("div",{staticClass:"grey lighten-3 mb-5"},[i("v-breadcrumbs",{staticClass:"pl-0 py-8 justify-center",attrs:{items:t.items},scopedSlots:t._u([{key:"item",fn:function(e){var s=e.item;return[i("v-breadcrumbs-item",{staticClass:"title",attrs:{to:{name:s.link},disabled:s.disabled,exact:""}},[t._v(" "+t._s(s.text.toUpperCase())+" ")])]}}])})],1)},a=[],r={name:"BreadCrumb",props:{items:{type:Array}},data:function(){return{}}},n=r,o=i("2877"),u=i("6544"),c=i.n(u),l=i("5530"),d=(i("a15b"),i("abd3"),i("ade3")),f=i("1c87"),m=i("58df"),h=Object(m["a"])(f["a"]).extend({name:"v-breadcrumbs-item",props:{activeClass:{type:String,default:"v-breadcrumbs__item--disabled"},ripple:{type:[Boolean,Object],default:!1}},computed:{classes:function(){return Object(d["a"])({"v-breadcrumbs__item":!0},this.activeClass,this.disabled)}},render:function(t){var e=this.generateRouteLink(),i=e.tag,s=e.data;return t("li",[t(i,Object(l["a"])(Object(l["a"])({},s),{},{attrs:Object(l["a"])(Object(l["a"])({},s.attrs),{},{"aria-current":this.isActive&&this.isLink?"page":void 0})}),this.$slots.default)])}}),v=i("80d2"),p=Object(v["i"])("v-breadcrumbs__divider","li"),b=i("7560"),g=Object(m["a"])(b["a"]).extend({name:"v-breadcrumbs",props:{divider:{type:String,default:"/"},items:{type:Array,default:function(){return[]}},large:Boolean},computed:{classes:function(){return Object(l["a"])({"v-breadcrumbs--large":this.large},this.themeClasses)}},methods:{genDivider:function(){return this.$createElement(p,this.$slots.divider?this.$slots.divider:this.divider)},genItems:function(){for(var t=[],e=!!this.$scopedSlots.item,i=[],s=0;s<this.items.length;s++){var a=this.items[s];i.push(a.text),e?t.push(this.$scopedSlots.item({item:a})):t.push(this.$createElement(h,{key:i.join("."),props:a},[a.text])),s<this.items.length-1&&t.push(this.genDivider())}return t}},render:function(t){var e=this.$slots.default||this.genItems();return t("ul",{staticClass:"v-breadcrumbs",class:this.classes},e)}}),y=Object(o["a"])(n,s,a,!1,null,null,null);e["a"]=y.exports;c()(y,{VBreadcrumbs:g,VBreadcrumbsItem:h})},abd3:function(t,e,i){}}]);
//# sourceMappingURL=CoworkingOffices.64dd0862.js.map